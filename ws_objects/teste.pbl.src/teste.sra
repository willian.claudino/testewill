﻿$PBExportHeader$teste.sra
$PBExportComments$Generated Application Object
forward
global type teste from application
end type
global transaction sqlca
global dynamicdescriptionarea sqlda
global dynamicstagingarea sqlsa
global error error
global message message
end forward

global type teste from application
string appname = "teste"
end type
global teste teste

on teste.create
appname = "teste"
message = create message
sqlca = create transaction
sqlda = create dynamicdescriptionarea
sqlsa = create dynamicstagingarea
error = create error
end on

on teste.destroy
destroy( sqlca )
destroy( sqlda )
destroy( sqlsa )
destroy( error )
destroy( message )
end on

